package com.example.demo;

import java.io.*;

import com.sun.xml.internal.ws.api.FeatureListValidatorAnnotation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PreFinalProjectApplication {
    @Value("${logging.file.name}")
    private static String loggingFilePath;

    public static void main(String[] args) throws FileNotFoundException {

        SpringApplication.run(PreFinalProjectApplication.class, args);
    }

}

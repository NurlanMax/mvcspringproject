package com.example.demo.controller;

import com.example.demo.model.dto.RegistrationForm;
import com.example.demo.model.Role;
import com.example.demo.model.User;
import com.example.demo.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.Collections;

@Controller
public class AdminController {
    private final UserService userService;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public AdminController(UserService userService, PasswordEncoder passwordEncoder) {
        this.userService = userService;
        this.passwordEncoder = passwordEncoder;
    }


    @GetMapping("/user-create")
    public String createUserForm(RegistrationForm form) {
        return "user-create";
    }

    @PostMapping("/user-create")
    public String createUser(RegistrationForm form) {
        userService.saveUser(form.toUser(passwordEncoder));
        return "redirect:/foradmin";
    }

    @GetMapping("/user-delete/{id}")
    public String deleteUser(@PathVariable("id") Long id) {
        userService.deleteById(id);
        return "redirect:/foradmin";
    }

    @GetMapping("/user-update/{id}")
    public String updateUserForm(@PathVariable("id") Long id, Model model) {
        User user = userService.findById(id);
        model.addAttribute("user", user);
        return "/user-update";
    }

    @PostMapping("/user-update")
    public String updateUser(User user) {
        user.setRole(user.getRole().toLowerCase());
        if (user.getRole().equals("admin")) {
            user.setRoles(Collections.singleton(Role.ADMIN));
        } else if (user.getRole().equals("user")) {
            user.setRoles(Collections.singleton(Role.USER));
        }
        user.setRole(null);
        user.setPassword(userService.findById(user.getId()).getPassword());
        userService.saveUser(user);
        return "redirect:/foradmin";
    }
}

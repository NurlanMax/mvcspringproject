package com.example.demo.controller;

import com.example.demo.model.User;
import com.example.demo.service.user.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Controller
public class AuthenticationController {
    private final UserServiceImpl userServiceImpl;

    @Autowired
    public AuthenticationController(UserServiceImpl userServiceImpl) {
        this.userServiceImpl = userServiceImpl;
    }

    @GetMapping
    public String index(@AuthenticationPrincipal User user, Model model) {
        if (user != null) {
            model.addAttribute("user", user.getUsername());
            return "index";
        }
        model.addAttribute("user", "anonymous");
        return "index";
    }

    @GetMapping("/login")
    public String login() {
        return "login";
    }

    @GetMapping("/foruser")
    public String forUser() {
        return "foruser";
    }


    @GetMapping("/foradmin")
    public String forAdmin(Model model) {
        List<User> users = userServiceImpl.findAll();
        model.addAttribute("t_customers", users);
        return "foradmin";
    }
}

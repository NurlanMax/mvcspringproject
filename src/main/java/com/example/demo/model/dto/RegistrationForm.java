package com.example.demo.model.dto;

import com.example.demo.model.Role;
import com.example.demo.model.User;
import lombok.Data;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Collections;

@Data
public class RegistrationForm {
    private String username;
    private String password;
    private String email;
    private String phone;
    private String role;

    public User toUser(PasswordEncoder passwordEncoder) {
        User user=new User();
        user.setEmail(email);
        user.setPassword(passwordEncoder.encode(password));
        user.setPhone(phone);
        user.setUsername(username);

        if(role==null) {
            user.setRoles(Collections.singleton(Role.USER));
        } else {
            role=role.toLowerCase();
            if(role.equals("admin")) {
                user.setRoles(Collections.singleton(Role.ADMIN));
            } else if (role.equals("user")) {
                user.setRoles(Collections.singleton(Role.USER));
            }
            else {
                user.setRoles(Collections.singleton(Role.USER));
            }
        }
        return user;
    }

}

package com.example.demo.service.user;

import com.example.demo.model.User;

public interface UserService {
    User findByUsername(String username);
    User saveUser(User user);
    void deleteById(Long id);
    User findById(Long user);
}

package com.example.demo.service.user;

import com.example.demo.model.User;
import com.example.demo.repo.UserRepo;
import com.example.demo.service.user.UserService;
import lombok.extern.slf4j.Slf4j;
import lombok.extern.slf4j.XSlf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class UserServiceImpl implements UserService {
    private final UserRepo userRepo;

    @Autowired
    public UserServiceImpl(UserRepo userRepo) {
        this.userRepo = userRepo;
    }


    @Override
    public User findById(Long id) {
        log.info("User findbyid successfully");
        return userRepo.findById(id).get();

    }

    public List<User> findAll() {
        log.info("method findAll implemented successfully");
        return userRepo.findAll();
    }

    @Override
    public User saveUser(User user) {
        log.info("method saveUser implemented successfully");
        return userRepo.save(user);
    }

    @Override
    public void deleteById(Long id) {
        userRepo.deleteById(id);
        log.info("user were deleted by id");
    }

    @Override
    public User findByUsername(String username) {
        log.info("method findByUser implemented successfully");
        return userRepo.findByUsername(username);
    }
}
